#cell segmentation template

#libraries
import os
from mplex_image import segment, codex
import skimage
from skimage import io, morphology
import pandas as pd
import numpy as np

###### job info ##############

s_sample = "SampleName"
s_slide_scene = "SceneName"
s_find = "FindDAPIString"

print(f'Processing {s_sample}')

nuc_diam=int
cell_diam=int
s_type = "cell_or_nuclei"

ls_markers = ['Ecad']
ls_rare = []

# directories 
codedir = 'PathtoCode'
subdir = f'{codedir}/SubtractedRegisteredImages/{s_sample}'
segdir = f'{codedir}/Segmentation/{s_sample}Cellpose_Segmentation'
segment.cmif_mkdir([segdir])

##############################################
#MATCHONLY
#segmentation

print(f'Predicting {s_type}')
if s_type == 'cell':
    [print(f'Using {item} for cell segmentation') for item in ls_markers]

os.chdir(subdir)
df_img = codex.parse_img()

# segment nuclei or cell
if s_type=='nuclei':
    d_img = segment.load_single(s_find, s_slide_scene)
    segment.save_img(d_img, segdir,s_type)
    print('segmenting nuclei')
    processed_list = []
    for key, img in d_img.items():
        d_result = segment.cellpose_nuc(key,img,diameter=nuc_diam)
        processed_list.append(d_result)

elif s_type=='cell':
    d_img = segment.load_stack(df_img,s_find,s_slide_scene,ls_markers,ls_rare)
    segment.save_img(d_img, segdir,s_type)
    print('segmenting cells')
    processed_list = []
    for key, img in d_img.items():
        d_result = segment.cellpose_cell(key,img,diameter=cell_diam)
        processed_list.append(d_result)

else:
        print('choose nuclei or cell')

#save results
segment.save_seg(processed_list,segdir,s_type)

#MATCHONLY
print('Done Segmenting with Cellpose!')

#match nuclei

print(f'Processing {s_slide_scene}')
os.chdir(f'{codedir}/Segmentation/{s_sample}Cellpose_Segmentation')

for s_file in os.listdir():
    if s_file.find(s_slide_scene) > -1:
        if s_file.find(f'{nuc_diam} - Nuclei Segmentation Basins.tif') > -1:
            labels = io.imread(s_file)
        elif s_file.find(f'{cell_diam} - Cell Segmentation Basins.tif') > -1: 
            cell_labels = io.imread(s_file)
        else:
            continue

#fill small holes
closing = morphology.closing(cell_labels)
#mathc nuclei
container,d_replace = segment.nuc_to_cell(labels,closing)

print('starting numba')
cells_relabel = segment.relabel_numba(container,closing)

#set background to zero
cells_zero = segment.zero_background(cells_relabel)
io.imsave(f'{s_slide_scene}_{".".join(ls_markers)} nuc{nuc_diam} matchedcell{cell_diam} - Cell Segmentation Basins.tif',cells_zero)

print('Done Matching Cells and Nuclei!')