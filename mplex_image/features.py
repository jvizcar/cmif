####
# title: features.py
# language: Python3.7
# date: 2020-06-00
# license: GPL>=v3
# author: Jenny
# description:
#   python3 script for single cell feature extraction
####

#libraries
import os
import sys
import numpy as np
import pandas as pd
import shutil
import skimage
import scipy
from scipy import stats
from scipy import ndimage as ndi
from skimage import measure, segmentation, morphology
from skimage import io, filters
import re
import json
from PIL import Image
Image.MAX_IMAGE_PIXELS = 1000000000

#functions
def extract_feat(labels,intensity_image, properties=('centroid','mean_intensity','area','eccentricity')):
    ''' 
    given labels and intensity image, extract features to dataframe
    '''
    props = measure.regionprops_table(labels,intensity_image, properties=properties)
    df_prop = pd.DataFrame(props)
    return(df_prop)

def expand_nuc(labels,distance=3):
    '''
    expand the nucelar labels by a fixed number of pixels
    '''
    boundaries = segmentation.find_boundaries(labels,mode='thick')
    shrunk_labels = labels.copy()
    shrunk_labels[boundaries] = 0
    background = shrunk_labels == 0
    distances, (i, j) = scipy.ndimage.distance_transform_edt(
                background, return_indices=True
            )

    grown_labels = labels.copy()
    mask = background & (distances <= distance)
    grown_labels[mask] = shrunk_labels[i[mask], j[mask]]
    ring_labels = grown_labels - shrunk_labels

    return(ring_labels, grown_labels) #shrunk_labels, grown_labels,

def contract_membrane(cell_labels,distance=3):
    '''
    expand the nucelar labels by a fixed number of pixels
    '''
    boundaries = segmentation.find_boundaries(cell_labels,mode='outer')
    shrunk_labels = cell_labels.copy()
    #morphology.remove_small_objects(cut_labels, i_small)
    shrunk_labels[boundaries] = 0
    foreground = shrunk_labels != 0
    distances, (i, j) = scipy.ndimage.distance_transform_edt(
                     foreground, return_indices=True
                 )

    mask = foreground & (distances <= distance)
    shrunk_labels[mask] = shrunk_labels[i[mask], j[mask]]
    rim_labels = cell_labels - shrunk_labels
    return(rim_labels)

def get_tumor_cyto(labels,cell_labels):
    '''
    given matched nuclear and cell label IDs,return cytoplasm minus nucleus
    '''
    overlap = cell_labels==labels
    ring_rep = cell_labels.copy()
    ring_rep[overlap] = 0
    return(ring_rep)

def get_mip(ls_img):
    '''
    maximum intensity projection of images (input list of filenames)
    '''
    imgs = []
    for s_img in ls_img:
        img = io.imread(s_img)
        imgs.append(img)
    mip = np.stack(imgs).max(axis=0)
    return(mip)

def thresh_li(img,area_threshold=100,low_thresh=1000):
    '''
    threshold an image with Li’s iterative Minimum Cross Entropy method
    if too low, apply the low threshold instead (in case negative)
    '''
    mask = img >= filters.threshold_li(img)
    mask = morphology.remove_small_holes(mask, area_threshold=area_threshold)
    mask[mask < low_thresh] = 0
    return(mask)

def mask_border(mask,type='inner',pixel_distance = 50):
    '''
    for inner, distance transform from mask to background
    for outer, distance transform from back ground to mask
    returns a mask
    '''
    shrunk_mask = mask.copy()
    if type == 'inner':
        foreground = ~mask
        background = mask
    elif type == 'outer':
        foreground = ~mask
        background = mask
    distances, (i, j) = scipy.ndimage.distance_transform_edt(
                background, return_indices=True
            )
    maskdist = mask & (distances <= pixel_distance)
    shrunk_mask[maskdist] = shrunk_mask[i[maskdist], j[maskdist]]
    mask_out = np.logical_and(mask,np.logical_not(shrunk_mask))
    return(mask_out,shrunk_mask,maskdist,distances)

def mask_labels(mask,labels):
    ''''
    return the labels that fall within the mask
    '''
    selected_array = labels[mask]
    a_unique = np.unique(selected_array)
    return(a_unique)

def parse_org(s_end = "ORG.tif",s_start='R'):
    """
    This function will parse images following koei's naming convention
    Example: Registered-R1_PCNA.CD8.PD1.CK19_Her2B-K157-Scene-002_c1_ORG.tif
    The output is a dataframe with image filename in index
    And rounds, color, imagetype, scene (/tissue), and marker in the columns
    """
    ls_file = []
    for file in os.listdir():
        if file.endswith(s_end):
            if file.find(s_start)==0:
                ls_file = ls_file + [file]
    df_img = pd.DataFrame(index=ls_file)
    df_img['rounds'] = [item.split('_')[0].split('Registered-')[1] for item in df_img.index]
    df_img['color'] = [item.split('_')[-2] for item in df_img.index]
    df_img['slide'] = [item.split('_')[2] for item in df_img.index]
    df_img['scene'] = [item.split('-Scene-')[1] for item in df_img.slide]
    #parse file name for biomarker
    for s_index in df_img.index:
        #print(s_index)
        s_color = df_img.loc[s_index,'color']
        if s_color == 'c1':
            s_marker = 'DAPI'
        elif s_color == 'c2':
            s_marker = s_index.split('_')[1].split('.')[0]
        elif s_color == 'c3':
            s_marker = s_index.split('_')[1].split('.')[1]
        elif s_color == 'c4':
            s_marker = s_index.split('_')[1].split('.')[2]
        elif s_color == 'c5':
            s_marker = s_index.split('_')[1].split('.')[3]
        else: print('Error')
        df_img.loc[s_index,'marker'] = s_marker
    return(df_img) 

def extract_cellpose_features(s_sample, segdir, subdir, ls_seg_markers, nuc_diam, cell_diam):
    '''
    load the segmentation results, the input images, and the channels images
    extract mean intensity from each image, and centroid, area and eccentricity for 
    '''

    df_sample = pd.DataFrame()
    df_thresh = pd.DataFrame()

    os.chdir(f'{segdir}/{s_sample}Cellpose_Segmentation')
    ls_scene = []
    d_match = {}
    for s_file in os.listdir():
        if s_file.find(f'{".".join(ls_seg_markers)} matchedcell{cell_diam} - Cell Segmentation Basins')>-1:
            ls_scene.append(s_file.split('_')[0])
            d_match.update({s_file.split('_')[0]:s_file})
        elif s_file.find(f'{".".join(ls_seg_markers)} nuc{nuc_diam} matchedcell{cell_diam} - Cell Segmentation Basins')>-1:
            ls_scene.append(s_file.split('_')[0])
            d_match.update({s_file.split('_')[0]:s_file})
    for s_scene in ls_scene:
        print(f'processing {s_scene}')
        for s_file in os.listdir():
            if s_file.find(s_scene) > -1:
                if s_file.find("DAPI.png") > -1:
                    s_dapi = s_file
        dapi = io.imread(s_dapi)
        print(f'loading {s_scene} nuclei{nuc_diam} - Nuclei Segmentation Basins.tif')
        labels = io.imread(f'{s_scene} nuclei{nuc_diam} - Nuclei Segmentation Basins.tif')
        cell_labels = io.imread(d_match[s_scene])
        print(f'loading {d_match[s_scene]}')
        #nuclear features
        df_feat = extract_feat(labels,dapi, properties=(['mean_intensity']))
        df_feat.columns = [f'{item}_segmented-nuclei' for item in df_feat.columns]
        df_feat.index = [f'{s_sample}_scene{s_scene.split("-Scene-")[1].split("_")[0]}_cell{item}' for item in df_feat.index]

        #get subcellular regions
        cyto = get_tumor_cyto(labels,cell_labels)
        membrane = contract_membrane(cell_labels,distance=3)
        peri_nuc3, __ = expand_nuc(labels,distance=3)
        peri_nuc5, __ = expand_nuc(labels,distance=5)
        d_loc = {'cytoplasm':cyto,'nuclei':labels,'membrane':membrane,'perinuc3':peri_nuc3,'perinuc5':peri_nuc5,'cell':cell_labels}
        #subdir organized by slide or scene
        if os.path.exists(f'{subdir}/{s_sample}'):
            os.chdir(f'{subdir}/{s_sample}')
        elif os.path.exists(f'{subdir}/{s_scene}'):
            os.chdir(f'{subdir}/{s_scene}')
        else:
            os.chdir(f'{subdir}')
        df_img = parse_org()
        df_img['round_int'] = [int(re.sub('[^0-9]','', item)) for item in df_img.rounds] 
        df_img = df_img[df_img.round_int < 90]
        df_img = df_img.sort_values('round_int')
        df_scene = df_img[df_img.scene==s_scene.split("-Scene-")[1].split("_")[0]]

        #load each image
        for s_index in df_scene.index:
                intensity_image = io.imread(s_index)
                df_thresh.loc[s_index,'threshold_li'] =  filters.threshold_li(intensity_image)
                s_marker = df_scene.loc[s_index,'marker']
                print(f'extracting features {s_marker}')
                if s_marker == 'DAPI':
                    s_marker = s_marker + f'{df_scene.loc[s_index,"rounds"].split("R")[1]}'
                for s_loc, a_loc in d_loc.items():
                    if s_loc == 'nuclei':
                        df_marker_loc = extract_feat(a_loc,intensity_image, properties=(['mean_intensity','centroid','area','eccentricity']))
                        df_marker_loc.columns = [f'{s_marker}_{s_loc}',f'{s_marker}_{s_loc}_centroid-0',f'{s_marker}_{s_loc}_centroid-1',f'{s_marker}_{s_loc}_area',f'{s_marker}_{s_loc}_eccentricity']
                    else:
                        df_marker_loc = extract_feat(a_loc,intensity_image, properties=(['mean_intensity']))
                        df_marker_loc.columns = [f'{s_marker}_{s_loc}']

                    #drop zero from array, set array ids as index
                    df_marker_loc.index = sorted(np.unique(a_loc)[1::])
                    df_marker_loc.index = [f'{s_sample}_scene{s_scene.split("-Scene-")[1].split("_")[0]}_cell{item}' for item in df_marker_loc.index]
                    df_feat = df_feat.merge(df_marker_loc, left_index=True,right_index=True,how='left',suffixes=('',f'{s_marker}_{s_loc}'))
        df_sample = df_sample.append(df_feat)
    return(df_sample, df_thresh)

def extract_cellpose_features_z(s_sample,ls_scene, segdir, subdir, ls_seg_markers, nuc_diam, cell_diam):
    '''
    For Guillaumes processed images (i.e. Z projection DAPI)
    load the segmentation results, the input images, and the channels images
    extract mean intensity from each image, and centroid, area and eccentricity for 
    '''

    df_sample = pd.DataFrame()
    df_thresh = pd.DataFrame()
    for s_scene in ls_scene:
        os.chdir(f'{segdir}/{s_sample}Cellpose_Segmentation')
        print(f'processing {s_scene}')

        if os.path.exists(f'{s_scene} nuclei{nuc_diam} - Nuclei Segmentation Basins.tif'):
            labels = io.imread(f'{s_scene} nuclei{nuc_diam} - Nuclei Segmentation Basins.tif')
        else:
            continue
        if os.path.exists(f'{s_scene} matchedcell{cell_diam} - Cell Segmentation Basins.tif'):
            cell_labels = io.imread(f'{s_scene} matchedcell{cell_diam} - Cell Segmentation Basins.tif')
        elif os.path.exists(f'{s_scene}_{".".join(ls_seg_markers)} matchedcell{cell_diam} - Cell Segmentation Basins.tif'):
            cell_labels = io.imread(f'{s_scene}_{".".join(ls_seg_markers)} matchedcell{cell_diam} - Cell Segmentation Basins.tif')
        else:
            continue
        for s_file in os.listdir():
            if s_file.find(s_scene) > -1:
                if s_file.find("DAPI.png") > -1:
                    s_dapi = s_file
                elif s_file.find('_CytoProj.png') > -1:
                    s_cyto = s_file
        dapi = io.imread(s_dapi)#(f'{s_scene} - ZProjectionDAPI.png')
        cyto_img = io.imread(s_cyto)#(f'{s_scene} - {".".join(ls_seg_markers)}_CytoProj.png')
        cyto_img = cyto_img[:,:,1]
        #nuclear features
        df_feat = extract_feat(labels,dapi, properties=(['mean_intensity']))
        df_feat.columns = [f'{item}_segmented-nuclei' for item in df_feat.columns]
        df_feat.index = [f'{s_sample}_scene{s_scene.split(" ")[1].split("_")[0]}_cell{item}' for item in df_feat.index]
        
        #cell features (skip)
        #df_cell = extract_feat(cell_labels,cyto_img, properties=('centroid','mean_intensity','area','eccentricity'))
        #df_cell.columns = [f'{item}_segmented-cells' for item in df_cell.columns]
        #df_cell.index = sorted(np.unique(cell_labels)[1::])
        #df_cell.index = [f'{s_sample}_scene{s_scene.split(" ")[1].split("_")[0]}_cell{item}' for item in df_cell.index]
        #df_feat = df_feat.merge(df_cell, left_index=True,right_index=True,how='left')
        #get subcellular regions
        cyto = get_tumor_cyto(labels,cell_labels)
        membrane = contract_membrane(cell_labels,distance=3)
        peri_nuc3, __ = expand_nuc(labels,distance=3)
        peri_nuc5, __ = expand_nuc(labels,distance=5)
        d_loc = {'cytoplasm':cyto,'nuclei':labels,'membrane':membrane,'perinuc3':peri_nuc3,'perinuc5':peri_nuc5,'cell':cell_labels}
        os.chdir(f'{subdir}/{s_sample}')
        df_img = parse_org()
        df_img['round_int'] = [int(re.sub('[^0-9]','', item)) for item in df_img.rounds] 
        df_img = df_img[df_img.round_int < 90]
        df_img = df_img.sort_values('round_int')
        df_scene = df_img[df_img.scene==s_scene.split(" ")[1].split('_')[0]]

        #load each image
        for s_index in df_scene.index:
            intensity_image = io.imread(s_index)
            df_thresh.loc[s_index,'threshold_li'] =  filters.threshold_li(intensity_image)
            s_marker = df_scene.loc[s_index,'marker']
            print(f'extracting features {s_marker}')
            if s_marker == 'DAPI':
                s_marker = s_marker + f'{df_scene.loc[s_index,"rounds"].split("R")[1]}'
            for s_loc, a_loc in d_loc.items():
                if s_loc == 'nuclei':
                    df_marker_loc = extract_feat(a_loc,intensity_image, properties=(['mean_intensity','centroid','area','eccentricity']))
                    df_marker_loc.columns = [f'{s_marker}_{s_loc}',f'{s_marker}_{s_loc}_centroid-0',f'{s_marker}_{s_loc}_centroid-1',f'{s_marker}_{s_loc}_area',f'{s_marker}_{s_loc}_eccentricity']
                else:
                    df_marker_loc = extract_feat(a_loc,intensity_image, properties=(['mean_intensity']))
                    df_marker_loc.columns = [f'{s_marker}_{s_loc}']

                #drop zero from array, set array ids as index
                df_marker_loc.index = sorted(np.unique(a_loc)[1::])
                df_marker_loc.index = [f'{s_sample}_scene{s_scene.split(" ")[1].split("_")[0]}_cell{item}' for item in df_marker_loc.index]
                df_feat = df_feat.merge(df_marker_loc, left_index=True,right_index=True,how='left',suffixes=('',f'{s_marker}_{s_loc}'))
        df_sample = df_sample.append(df_feat)
    return(df_sample, df_thresh)

def combine_labels(s_sample,segdir, subdir, ls_seg_markers, nuc_diam, cell_diam, se_neg):
    '''
    - load cell labels; delete cells that were not used for cytoplasm (i.e. ecad neg)
    - nuc labels, expand to perinuc 5 and then cut out the cell labels
    - keep track of cells that are completely coverd by another cell (or two or three: counts as touching).
    '''
    dd_result = {}
    os.chdir(f'{segdir}/{s_sample}Cellpose_Segmentation')
    ls_scene = []
    for s_file in os.listdir():
        if s_file.find(' - DAPI.png') > -1:
            ls_scene.append(s_file.split(' - DAPI.png')[0])
    for s_scene in ls_scene:
        se_neg_scene = se_neg[se_neg.index.str.contains(s_scene.replace("Scene ","scene")) | se_neg.index.str.contains(s_scene.replace("-Scene-","_scene"))]

        print(f'processing {s_scene}')
        if os.path.exists(f'{s_scene} nuclei{nuc_diam} - Nuclei Segmentation Basins.tif'):
            labels = io.imread(f'{s_scene} nuclei{nuc_diam} - Nuclei Segmentation Basins.tif')
        else:
            print('no nuclei labels found')
        if os.path.exists(f'{s_scene} matchedcell{cell_diam} - Cell Segmentation Basins.tif'):
            cell_labels = io.imread(f'{s_scene} matchedcell{cell_diam} - Cell Segmentation Basins.tif')
        elif os.path.exists(f'{s_scene}_{".".join(ls_seg_markers)} matchedcell{cell_diam} - Cell Segmentation Basins.tif'):
            cell_labels = io.imread(f'{s_scene}_{".".join(ls_seg_markers)} matchedcell{cell_diam} - Cell Segmentation Basins.tif')
        elif os.path.exists(f'{s_scene}_{".".join(ls_seg_markers)} nuc{nuc_diam} matchedcell{cell_diam} - Cell Segmentation Basins.tif'):
            cell_labels = io.imread(f'{s_scene}_{".".join(ls_seg_markers)} nuc{nuc_diam} matchedcell{cell_diam} - Cell Segmentation Basins.tif')
        else:
            print('no cell labels found')
        #set non-ecad cell labels to zero
        a_zeros = np.array([int(item.split('_cell')[1]) for item in se_neg_scene[se_neg_scene].index]).astype('int64')
        mask = np.isin(cell_labels, a_zeros)
        cell_labels_copy = cell_labels.copy()
        cell_labels_copy[mask] = 0
        #make the nuclei under cells zero
        labels_copy = labels.copy()
        distance = 5
        perinuc5, labels_exp = expand_nuc(labels,distance=distance)
        labels_exp[cell_labels_copy > 0] = 0
        #combine calls and expanded nuclei
        combine = (labels_exp + cell_labels_copy)
        if s_scene.find('Scene') == 0:
            io.imsave(f'{s_sample}_{s_scene.replace("Scene ","scene")}_cell{cell_diam}_nuc{nuc_diam}_CombinedSegmentationBasins.tif',combine)
        else:
            io.imsave(f'{s_scene}_{".".join(ls_seg_markers)}-cell{cell_diam}_exp{distance}_CellSegmentationBasins.tif',combine)
        #figure out the covered cells...labels + combined
        not_zero_pixels =  np.array([labels.ravel() !=0,combine.ravel() !=0]).all(axis=0)
        a_tups = np.array([combine.ravel()[not_zero_pixels],labels.ravel()[not_zero_pixels]]).T #combined over nuclei
        unique_rows = np.unique(a_tups, axis=0)
        new_dict = {}
        for key, value in unique_rows:
            if key == value:
                continue
            else:
                if key in new_dict:
                    new_dict[key].append(value)
                else:
                    new_dict[key] = [value]
        #from elmar (reformat cells touching dictionary and save
        d_result = {}
        for i_cell, li_touch in new_dict.items():
            d_result.update({str(i_cell): [str(i_touch) for i_touch in li_touch]})
        dd_result.update({f'{s_sample}_{s_scene.replace("Scene ","scene")}':d_result})
    #save dd_touch as json file
    with open(f'result_{s_sample}_cellsatop_dictionary.json','w') as f: 
        json.dump(dd_result, f)

    return(labels,combine,dd_result)