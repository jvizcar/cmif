# image processing for with mlpex_image
# date: 2020-08-18
# author: engje
# language Python 3.8
# license: GPL>=v3
# Modified by JC Vizcarra

# Import libraries
import os
from mplex_image import preprocess, metadata, mpimage, cmif
import javabridge
import bioformats
import sys
import numpy as np
import pandas as pd
import shutil
import matplotlib.pyplot as plt
import re
from skimage import io

# sections dict - set key to True to run specific sections
run = dict(
    QC_CZIS=False,
    QC_TIFFS=False,
    REGISTER=False,
    AF_SUBTRACT=True
)

# Set Paths
codedir = os.getcwd()
rootdir = f'{codedir}/data/PipelineExample'
tiffdir = f'{rootdir}/RawImages'
qcdir = f'{rootdir}/QC'
regdir = f'{rootdir}/RegisteredImages'
subdir = f'{rootdir}/SubtractedRegisteredImages'
segdir = f'{rootdir}/Segmentation'
czidir = f'{rootdir}/czis'

# Start Preprocessing
preprocess.cmif_mkdir([tiffdir, qcdir, regdir, segdir, subdir])

# List slides to be processed
ls_sample = ['BC44290-146']

if run['QC_CZIS']:
    javabridge.start_vm(class_path=bioformats.JARS)

    for s_sample in ls_sample:
        os.chdir(f'{czidir}/{s_sample}')
        # 1 rename undescores to dot to match convention (done)
        d_rename = mpimage.underscore_to_dot(s_sample, s_end='.czi')
        d_rename.update({'HER2_ER': 'HER2.ER'})
        # preprocess.dchange_fname(d_rename) #,b_test=False)

        # 2 Check files/naming
        df_img = cmif.parse_czi(f'{czidir}/{s_sample}', b_scenes=True)
        cmif.count_images(df_img)
        preprocess.check_names(df_img, s_type='czi')

        # 3 Export useful imaging metadata (done)
        df_img = metadata.scene_position(f'{czidir}/{s_sample}', type='r')
        df_img.to_csv(f'{codedir}/{s_sample}_ScenePositions.csv')
        metadata.exposure_times_scenes(df_img, rootdir, czidir=f'{czidir}/{s_sample}', s_end='.czi')

    javabridge.kill_vm()

if run['QC_TIFFS']:
    # Raw tiffs: check/change names
    for s_sample in ls_sample:
        os.chdir(f'{tiffdir}/{s_sample}')

        # sort and count images
        df_img = mpimage.parse_org(s_end="ORG.tif", type='raw')
        cmif.count_images(df_img[df_img.slide == s_sample])

    # QC Raw tiffs: visual inspection #
    preprocess.cmif_mkdir([f'{qcdir}/RawImages'])

    for s_sample in ls_sample:
        os.chdir(f'{tiffdir}/{s_sample}')

        # investigate tissues
        df_img = mpimage.parse_org(s_end="ORG.tif", type='raw')
        cmif.visualize_raw_images(df_img, qcdir, color='c1')

if run['REGISTER']:
    for s_sample in ls_sample:
        cmif.registration_python(s_sample, tiffdir, regdir, qcdir)

    # visualize the registration results
    cmif.visualize_reg_images(f'{regdir}', qcdir, color='c1')

if run['AF_SUBTRACT']:
    # parameters
    d_channel = {'c2': 'R8Qc2', 'c3': 'R8Qc3', 'c4': 'R8Qc4', 'c5': 'R8Qc5'}

    for s_sample in ls_sample:
        preprocess.cmif_mkdir([f'{subdir}/{s_sample}'])
        os.chdir(f'{regdir}/{s_sample}-Scene-1')
        df_img = mpimage.parse_org()
        ls_exclude = sorted(set(df_img[df_img.color == 'c5'].marker)) + ['DAPI'] + [item for key, item in
                                                                                    d_channel.items()]
        # subtract
        df_img, df_exp, df_markers, df_copy = cmif.autofluorescence_subtract(regdir, f'{codedir}/data/PipelineExample',
                                                                             d_channel, ls_exclude,
                                                                             subdir=f'{subdir}/{s_sample}')
    # generate channel/marker metadata csv
    cmif.segmentation_inputs(subdir, segdir, d_segment={}, tma_bool=True, b_start=False, i_counter=0)

